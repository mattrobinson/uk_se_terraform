#------------------------------------------------------------------------------
# Variables used by Terraform Config. 
#------------------------------------------------------------------------------
#
# *** Important - Do not put credentials in this file. ***  
#
#------------------------------------------------------------------------------

variable "prefix" {
  description = "prefix for resources created"
  default     = "mattr"
}

# DEFINE UK SE NAME FOR USE IN TAGS
variable "uk_se_name" {
  type    = string
  default = "mattr"
}

# DEFINE AWS REGION
variable "aws_region" {
  type    = string
  default = "eu-west-2"
}


# DEFINE SSH KEY (AS NAMED IN AWS) FOR AWS AMI BUILD
variable "ssh_key_name" {
  type    = string
  default = "MattR-key"
}


# DEFINE SSH KEY (LOCAL PATH) FOR AWS AMI BUILD
#variable "ssh_key_file_path" {
#  type    = string
#  default = "~/.ssh/id_rsa"
#}

# DEFINE AWS VPC NAME
variable "vpc_name" {
  type    = string
  default = "mattr-f5-vpc"
}

variable "f5_ami_search_name" {
  type    = string
  default = "F5*BIGIP-15.1.0.4*PAYG-Better*25Mbps*"
}

variable "libs_dir" {  
  description = "Destination directory on the BIG-IP to download the A&O Toolchain RPMs"  
  type        = string  
  default     = "/config/cloud/aws/node_modules"
}

variable "onboard_log" {  
  description = "Directory on the BIG-IP to store the cloud-init logs"  
  type        = string  
  default     = "/var/log/startup-script.log"
}

variable "username" {
  description = "username for BIGIP"
  type = string
  default = "admin"
}

variable "port" {
  description = "TCP port for management interface of BIGIP"
  type = string
  default = "443"
}

#------------------------------------------------------------------------------